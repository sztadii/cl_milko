var banner;

banner = new function () {

  //catch DOM
  var $el;
  var $slider;
  var $sliderContent;
  var $sliderCow;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $('.banner');
    $slider = $el.find('.banner__slider');
    $sliderCow = $el.find('.banner__cow');
    $sliderContent = $el.find('.banner__content');

    sliderMake();
  };

  var sliderMake = function () {

    if ($slider.length > 0) {
      $slider.imagesLoaded({background: true}).always(function () {
        $slider.slick({
          infinite: true,
          dots: false,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: true,
          adaptiveHeight: true,
          autoplay: true,
          speed: 800,
          autoplaySpeed: 4000,
          draggable: false,
          pauseOnHover: false,
          pauseOnFocus: false
        });

        $slider.on('beforeChange', function () {
          $sliderCow.addClass('-hide');
          $sliderContent.addClass('-hide');
        });

        $slider.on('afterChange', function () {
          $sliderCow.removeClass('-hide');
          $sliderContent.removeClass('-hide');
        });
      });
    }
  };
};
