var nav;

nav = new function () {

  //private vars
  var breakpoint = 1200;
  var windowWidth;
  var windowScrollTop;

  //catch DOM
  var $el;
  var $button;
  var $background;
  var $listMain;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).resize(function () {
    varsUpdate();
  });

  $(window).scroll(function () {
    varsUpdate();
    navLinkActive();
  });

  //private functions
  var init = function () {
    domCatch();
    varsUpdate();
    navTrigger();
    navLinkActive();
  };

  var domCatch = function () {
    $el = $('.nav');
    $button = $('.nav__button');
    $background = $('.nav .nav__background');
    $listMain = $('.nav .nav__list');
  };

  var varsUpdate = function () {
    windowWidth = $(window).outerWidth();
    windowScrollTop = $(window).scrollTop();
  };

  var navTrigger = function () {
    $button.on('click', function () {
      navToggle();
    });

    $background.on('click', function () {
      navToggle();
    });

    $('.nav__link').on('click', function () {
      navToggle();
    });
  };

  var navHide = function () {
    $background.fadeOut(500);
    $listMain.fadeOut(400, function () {
      $button.removeClass('-active');
    });
  };

  var navShow = function () {
    $background.fadeIn(500);
    $listMain.fadeIn(400, function () {
      $button.addClass('-active');
    });
  };

  var navToggle = function () {
    if (breakpoint > windowWidth) {
      if ($button.hasClass('-active')) {
        navHide();
      } else {
        navShow();
      }
    }
  };

  var navLinkActive = function () {
    var $sections = $('main > div');
    var $navLink = $('.nav__link');

    $sections.each(function (i) {

      if ($(this).position().top <= windowScrollTop) {

        if ($navLink.hasClass('-active'))
          $navLink.removeClass('-active');

        $navLink.eq(i).addClass('-active');
      }
    });
  };
};