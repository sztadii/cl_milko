var gallery;

gallery = new function () {

  //catch DOM
  var $el;
  var $box;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".gallery");
    $box = $el.find(".gallery__box");

    if ($box.length) {
      $box.imagesLoaded({background: true}).always(function () {
        $box.lightGallery({
          mode: 'lg-slide'
        });

        $box.each(function () {
          $(this).slick({
            infinite: false,
            dots: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            adaptiveHeight: true,
            autoplay: true,
            speed: 1000,
            autoplaySpeed: 1500,
            draggable: false,
            pauseOnHover: false,
            pauseOnFocus: false,
            customPaging: function (a, b) {
              return '<div class="slick__dot"></div>';
            },
            appendDots: $(this).parent()
          });
        });
      });
    }
  };
};