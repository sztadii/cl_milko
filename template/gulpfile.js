'use strict';

var gulp = require('gulp');
var csso = require('gulp-csso');
var rimraf = require('gulp-rimraf');
var ignore = require('gulp-ignore');
var htmlmin = require('gulp-htmlmin');
var gcmq = require('gulp-group-css-media-queries');
var gulpif = require('gulp-if');
var sass = require('gulp-sass');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var bulkSass = require('gulp-sass-bulk-import');
var shorthand = require('gulp-shorthand');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var pug = require('gulp-pug');
var useref = require('gulp-useref');
var browser = require('browser-sync').create();
var wiredep = require('wiredep').stream;
var sequence = require('run-sequence');
var inject = require('gulp-inject-string');
var data = require('gulp-data');
var prettify = require('gulp-jsbeautifier');
var path = require('path');

var fs = require('fs');
var config = JSON.parse(fs.readFileSync('./config.json'));

var PORT = new function () {
  this.prod = config.PORT.prod;
  this.dev = config.PORT.dev;
  this.ui = config.PORT.ui;
};

var DIST = new function () {
  this.dir = config.DIST.dir;
  this.assetsDir = config.DIST.dir + config.DIST.assetsDir;
  this.cssDir = config.DIST.dir + config.DIST.cssDir;
  this.imgDir = config.DIST.dir + config.DIST.imgDir;
  this.fontsDir = config.DIST.dir + config.DIST.fontsDir;
  this.jsDir = config.DIST.dir + config.DIST.jsDir;
  this.cssDevFile = config.DIST.cssDevFile;
  this.jsDevFile = config.DIST.jsDevFile;
  this.cssProdFile = config.DIST.cssProdFile;
  this.jsProdFile = config.DIST.jsProdFile;
};

var COMPATIBILITY = config.COMPATIBILITY;

var PATHS = new function () {
  this.styles = config.PATHS.styles;
  this.scripts = config.PATHS.scripts;
};

// Copy img to the "dist" folder
gulp.task('images', function () {
  return gulp.src(['src/utils/img/*', 'src/components/**/src/img/*'])
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest(DIST.imgDir));
});

// Copy fonts to the "dist" folder
gulp.task('fonts', function () {
  return gulp.src(['src/utils/fonts/*', 'src/components/**/src/fonts/*'])
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest(DIST.fontsDir));
});

// Delete the "dist" folder
// This happens every time a build starts
gulp.task('clean', function () {
  return gulp.src(DIST.dir, {read: false})
    .pipe(rimraf());
});

gulp.task('clean:img', function () {
  return gulp.src(DIST.imgDir + '*', {read: false})
    .pipe(rimraf());
});

gulp.task('clean:fonts', function () {
  return gulp.src(DIST.fontsDir + '*', {read: false})
    .pipe(rimraf());
});

gulp.task('clean:pages', function () {
  return gulp.src(DIST.dir + '**.html', {read: false})
    .pipe(ignore(DIST.assetsDir))
    .pipe(rimraf());
});

gulp.task('clean:dev', function () {
  var devs = [
    DIST.jsDir + DIST.jsDevFile,
    DIST.cssDir + DIST.cssDevFile
  ];
  return gulp.src(devs, {read: false})
    .pipe(rimraf());
});

// Compile Sass into CSS
gulp.task('sass', function () {
  return gulp.src(PATHS.styles)
    .pipe(bulkSass())
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: COMPATIBILITY
    }))
    .pipe(sourcemaps.write())
    .pipe(rename(DIST.cssDevFile))
    .pipe(gulp.dest(DIST.cssDir));
});

// Combine JavaScript into one file
gulp.task('javascript', function () {
  return gulp.src(PATHS.scripts)
    .pipe(inject.prepend('"use strict";'))
    .pipe(sourcemaps.init())
    .pipe(concat(DIST.jsDevFile))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(DIST.jsDir));
});

// Make pug pages
gulp.task('pages', function () {
  return gulp.src('src/pages/*.pug')
    .pipe(data(function (file) {
      return {'config': require('./config.json')};
    }))
    .pipe(pug())
    .pipe(prettify({
      indent_size: 4,
      indent_char: ' '
    }))
    .pipe(gulp.dest(DIST.dir))
    .pipe(wiredep())
    .pipe(gulp.dest(DIST.dir));
});

gulp.task('buildProd', function (done) {
  sequence('buildProdAssets', 'clean:dev', done);
});

gulp.task('buildProdAssets', function () {

  var mini = imagemin({
    progressive: true
  });

  return gulp.src(DIST.dir + '**/*.html')
    .pipe(useref())
    .pipe(gulpif(DIST.imgDir + '**/*', mini))
    .pipe(gulpif('**/*.js', uglify().on('error', function (e) {
      console.log(e);
    })))
    .pipe(gulpif('**/*.css', gcmq()))
    .pipe(gulpif('**/*.css', shorthand()))
    .pipe(gulpif('**/*.css', csso()))
    .pipe(gulp.dest(DIST.dir));
});

gulp.task('buildProdHtml', function () {
  var optionsHtml = {
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true,
    removeComments: true,
    removeAttributeQuotes: true,
    removeEmptyAttributes: true,
    removeStyleLinkTypeAttributes: true
  };

  return gulp.src(DIST.dir + '*.html')
    .pipe(htmlmin(optionsHtml))
    .pipe(gulp.dest(DIST.dir));
});

// Build the "dist" folder by running all of the above tasks
gulp.task('build', function (done) {
  sequence('clean', ['images', 'fonts', 'sass', 'javascript'], 'pages', done);
});

// Start a server with LiveReload to preview the site in
gulp.task('server', ['build'], function () {
  browser.init({
    startPath: DIST.dir,
    server: {
      baseDir: './'
    },
    port: PORT.dev,
    index: 'index.html',
    open: false,
    ui: {
      port: PORT.ui
    },
    reloadDelay: 0,
    reloadOnRestart: true
  });
});

gulp.task('review', function () {
  var htmlSizeAll = 0, htmlSizeFirst = 0, cssSize = 0, jsSize = 0, imgSizeAll = 0;

  var distFiles = fs.readdirSync(DIST.dir);
  for (var i in distFiles) {
    if (path.extname(distFiles[i]) === ".html") {
      if(htmlSizeFirst == 0)
        htmlSizeFirst = fs.statSync(DIST.dir + distFiles[i])["size"];
      htmlSizeAll += fs.statSync(DIST.dir + distFiles[i])["size"];
    }
  }

  var images = fs.readdirSync(DIST.imgDir);
  for (var i in images) {
    if (path.extname(images[i])) {
      imgSizeAll += fs.statSync(DIST.imgDir + images[i])["size"];
    }
  }

  var styles = fs.readdirSync(DIST.cssDir);
  for (var i in styles) {
    if (path.extname(styles[i])) {
      cssSize += fs.statSync(DIST.cssDir + styles[i])["size"];
    }
  }

  var scripts = fs.readdirSync(DIST.jsDir);
  for (var i in scripts) {
    if (path.extname(scripts[i])) {
      jsSize += fs.statSync(DIST.jsDir + scripts[i])["size"];
    }
  }

  console.log(' ');
  console.log('----------------------------------');
  console.log('REVIEW: ');
  console.log('HTML FIRST: ' + htmlSizeFirst / 1000.0 + ' kB');
  console.log('HTML ALL:   ' + htmlSizeAll / 1000.0 + ' kB');
  console.log('CSS:        ' + cssSize / 1000.0 + ' kB');
  console.log('JS:         ' + jsSize / 1000.0 + ' kB');
  console.log('IMG ALL:    ' + imgSizeAll / 1000.0 + ' kB');
  console.log('----------------------------------');
  console.log(' ');
});

gulp.task('watch', function () {
  gulp.watch('src/**/*.pug', {cwd: './'}, ['clean:pages', 'pages', browser.reload]);
  gulp.watch('src/**/*.sass', {cwd: './'}, ['sass', browser.reload]);
  gulp.watch('src/**/*.js', {cwd: './'}, ['javascript', browser.reload]);
  gulp.watch(['src/utils/img/*', 'src/components/**/src/img/*'], {cwd: './'}, ['clean:img', 'images', browser.reload]);
  gulp.watch(['src/utils/fonts/*', 'src/components/**/src/fonts/*'], {cwd: './'}, ['clean:fonts', 'fonts', browser.reload]);
  gulp.watch('src/components/*', ['build', 'watch']);
});

// Build the site, run the server, and watch for file changes
gulp.task('default', ['build', 'server', 'watch']);